#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <math.h>

#include <stdint.h>

typedef struct P{
    double* x;
    double* v;
    double* best;
} P;

//from the function file
double function();
int dim;

int main(int argc, const char* argv[]){
    //just assume the parameters are there
    int n = 8;
    int runs = 1000;
    //yes we're using a square box
    int* limits = (int*)calloc(2*dim, sizeof(int));

    if(argc == 3){
        n = atoi(argv[1]);
        for(int i = 0; i < dim; i++){
            limits[2*i + 0] = -1;
            limits[2*i + 1] = 1;
        }
        runs = atoi(argv[2]);
    }
    else if(argc == 3 + 2*dim){
        n = atoi(argv[1]);
        for(int i = 3; i < 3+2*dim; i++){
            limits[i-3] = atoi(argv[i]);
        }
        runs = atoi(argv[2]);
    }
    else{
        printf("Wrong format, buddy\n");
        return -1;
    }
    

    srand(time(NULL));
    double W = 1;
    double Rp = 2;
    double Rg = 2;
    
    //initialize the swarm
    P* swarm = (P*)calloc(n, sizeof(P));
    double* global = NULL;
    for(int i = 0; i < n; i++){
        swarm[i].x = (double*)calloc(dim, sizeof(double));
        swarm[i].v = (double*)calloc(dim, sizeof(double));
        swarm[i].best = (double*)calloc(dim, sizeof(double));

        for(int d = 0; d < dim; d++){
            swarm[i].x[d] = (double)rand()/RAND_MAX * 
                (limits[2*d + 1] - limits[2*d + 0]) +
                limits[2*d + 0];

            swarm[i].v[d] = (double)rand()/RAND_MAX *
                2*(limits[2*d + 1] - limits[2*d + 0]) -
                (limits[2*d + 1] - limits[2*d + 0]);

            swarm[i].best[d] = swarm[i].x[d];
        }

        //get global
        if(global == NULL)
            global = swarm[i].best;
        else if(function(swarm[i].best) < function(global))
            global = swarm[i].best;
    }

    //double last = TMP_MAX;
    struct timespec end, start;
    clock_gettime(CLOCK_MONOTONIC_RAW, &start);

    int iter = 0;
    int r = 0;
    //double precision
    while(r++ < runs){
        //last = function(global);
        for(int p = 0; p < n; p++){
            for(int d = 0; d < dim; d++){
                double rp = (double)rand()/RAND_MAX; 
                double rg = (double)rand()/RAND_MAX; 
                swarm[p].v[d] = W*swarm[p].v[d] + 
                    Rp*rp*(swarm[p].best[d] - swarm[p].x[d]) + 
                    Rg*rg*(global[d] - swarm[p].x[d]);
                
                swarm[p].x[d] = swarm[p].x[d] + swarm[p].v[d];
                //limit check
                if(swarm[p].x[d] < limits[2*d + 0]){
                    swarm[p].x[d] = limits[2*d + 0];
                    swarm[p].v[d] = 0;
                }
                else if(swarm[p].x[d] > limits[2*d + 1]){
                    swarm[p].x[d] = limits[2*d + 1];
                    swarm[p].v[d] = 0;
                }
            }
            if(function(swarm[p].x) < function(swarm[p].best)){
                for(int q = 0; q < dim; q++)
                    swarm[p].best[q] = swarm[p].x[q];
            }
            if(function(swarm[p].x) < function(global)){
                for(int q = 0; q < dim; q++)
                    global[q] = swarm[p].x[q];
                iter = r; 
            }
        }
    }
    clock_gettime(CLOCK_MONOTONIC_RAW, &end);

    printf("Solution: (");
    for(int d = 0; d < dim; d++)
        printf("%f, ", global[d]);
    printf(")\nValue: %f\n", function(global));

    uint64_t delta = (end.tv_sec - start.tv_sec) * 1000000 + (end.tv_nsec - start.tv_nsec) / 1000;

    printf("Global found after %d iterations\n", iter);
    printf("Took %ld microseconds\n", delta);
}
