#include <stdio.h>

int dim = 4;
double function(double* x){
    double res = 0.0; 
    for(int i = 0; i < dim; i++)
        res += x[i]*x[i];
    return res;
};

