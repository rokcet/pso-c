#
# Makefile 
#

CC = gcc
CFLAGS = -g -Wall -std=c99 -pthread
EXES = x2 x3 x4

all: $(EXES)

x2: x2.c pso.o
	$(CC) $(CFLAGS) x2.c pso.o -o x2

x3: x3.c pso.o
	$(CC) $(CFLAGS) x3.c pso.o -o x3 -lm

x4: x4.c pso.o
	$(CC) $(CFLAGS) x4.c pso.o -o x4 -lm

pso.o: pso.c
	$(CC) $(CFLAGS) -c pso.c

clean:
	-rm -f *.o $(EXES) pso 
